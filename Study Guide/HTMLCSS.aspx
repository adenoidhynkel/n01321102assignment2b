﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="HTMLCSS.aspx.cs" Inherits="Study_Guide.HTMLCSS" %>
<asp:Content ID="cssPageIntro" ContentPlaceHolderID="MainContent" runat="server">
    <div class="jumbotron">
        <h1>Cascading Style Sheets (CSS)</h1>
        <p>CSS is pretty straightforward conceptually, however, once you start practising it, you realize it is not as easy as it looks.
            Floats and clear are both extremely useful tools to use when you are styling a page that is not cooperating with your vision. In fact, I
            cannot imagine styling a page without them. The float property specifies where an element should go.
            Clear is a helpful property when you have multiple float properties and they are interfering with other elements on the page. Clear specifies
            what elements can float beside it. For example, "clear: left" clears an element that is to the left.
        </p>
    </div>
</asp:Content>
<asp:Content ID="myCssCodeExample" ContentPlaceHolderID="CodeExample" runat="server">
     <cbx:CodeBox ID="myCSSEx" runat="server" Code="my_css_code" Owner="me"></cbx:CodeBox>  
</asp:Content>
<asp:Content ID="cssCodeLesson" ContentPlaceHolderID="CodeLesson" runat="server">
     <cbx:CodeBox ID="CSSEx" runat="server" Code="css_example" Owner="w3schools"></cbx:CodeBox> 
</asp:Content>
<asp:Content ID="cssCodeDescription" ContentPlaceHolderID="CodeResources" runat="server">
    <h4>Description</h4>
    <p>
    What my code demonstrates is the use of floats and clears in CSS. There are multiple elements floating left and right.
    I had to use "clear:both" in the footer to push it to the bottom of the page. Otherwise, it was overlapping with
    the other content because the floats in place altered the original layout of the page. This establishes that both
    sides around the footer should have nothing beside it.
    </p>
</asp:Content>
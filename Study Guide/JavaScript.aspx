﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="JavaScript.aspx.cs" Inherits="Study_Guide.JavaScript" %>

<asp:Content ID="jsPageIntro" ContentPlaceHolderID="MainContent" runat="server">
       <div class="jumbotron">
        <h1>JavaScript</h1>
            <p class="col-">An interesting and convenient concept I've learned in JavaScript is loops. Loops allow us to cycle through a line of code or condition
            as long as the specified conditions are met. The types of loops we have been using are for and while loops. A for loop has three components to it:
            assigning the variable, setting the condition, and setting the incrementor. A while loop only has one component in the brackets, which is setting
            the condition.
            </p>
        </div>
</asp:Content>

 <asp:Content ID="myJsCodeExample" ContentPlaceHolderID="CodeExample" runat="server">        
    <cbx:CodeBox ID="myJsCode" runat="server" Code="my_javascript" Owner="me"></cbx:CodeBox>
</asp:Content>

<asp:Content ID="jsCodeLesson" ContentPlaceHolderID="CodeLesson" runat="server">
    <cbx:CodeBox ID="jsCodeExample" runat="server" Code="javascript_example" Owner="W3Schools"></cbx:CodeBox>
</asp:Content>

<asp:Content ID="jsCodeDescription" ContentPlaceHolderID="CodeResources" runat="server">
<h4>Description</h4>
    <p> 
    My code is one of my JavaScript assignments, where I tried both a for and while loop to have a good understanding of how each works.
    This assignment accesses an array of books numbered from 1 - 10. There are 10 items, however, arrays start counting at 0. In order for my array to select the first
    book when the user inputs 1 and not 0, I added "[userInput - 1]" to the last line of code. That way, whatever the user inputs in will subtract by 1 when picking the
    array item from 0 - 9.
    </p>
</asp:Content>

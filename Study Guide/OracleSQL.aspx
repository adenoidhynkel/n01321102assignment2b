﻿<%@ Page Title="Contact" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="OracleSQL.aspx.cs" Inherits="Study_Guide.OracleSQL" %>

<asp:Content ID="sqlPageIntro" ContentPlaceHolderID="MainContent" runat="server">

    <div class="jumbotron">
        <h1>Oracle SQL</h1>
        <p>Oracle SQL is a useful language for pulling and arranging data, as well as creating tables. One of the most difficult things I have learned in SQL is
        joins. The concept is not that difficult, however, knowing how and when to use a type of join is more tricky. For example, left and right joins are best
        used when you want to compare data between two tables and see the data that does not match. A full join is best used when you want to merge the data that
        is common between two tables.
        </p>
    </div>

</asp:Content>
<asp:Content ID="mySqlCodeExample" ContentPlaceHolderID="CodeExample" runat="server">
      <cbx:CodeBox ID="mySQLcode" runat="server" Code="my_sql" Owner="me"></cbx:CodeBox>
</asp:Content>
<asp:Content ID="sqlCodeLesson" ContentPlaceHolderID="CodeLesson" runat="server">
      <cbx:CodeBox ID="SQLex" runat="server" Code="sql_example" Owner="Teacher"></cbx:CodeBox>
</asp:Content>
<asp:Content ID="sqlCodeDescription" ContentPlaceHolderID="CodeResources" runat="server">
<h4>Description</h4>
    <p> 
    My Code is an example of a full join. Here, we are selecting the invoice ids and vender names from the invoices table and vendors table.
    In order to do this, we must full join the vendors table. This will join all data similarities in these columns. For this, I used alaises i and v to distinguish
    which columns are coming from vendors and invoices. We joined on vendor id because both tables have that column. This query will output the invoice id and
    vendor name of the vendor ids that are common between the two tables.
    </p>
</asp:Content>

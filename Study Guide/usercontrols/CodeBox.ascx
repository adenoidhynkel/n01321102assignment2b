﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CodeBox.ascx.cs" Inherits="Study_Guide.usercontrols.CodeBox" %>
<div class="code-box-wrapper">
<h2 id="codeOwner" runat="server"></h2>
<asp:DataGrid id="myCodeBoxUC" runat="server" CssClass="cb-style">
    <HeaderStyle CssClass="cb-header" />
    <ItemStyle CssClass="cb-item" /> 
</asp:DataGrid>
</div>
﻿using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Study_Guide.usercontrols
{
    public partial class CodeBox : System.Web.UI.UserControl
    {

        public string Code
        {
            get { return (string)ViewState["Content"]; }
            set { ViewState["Content"] = value; }
        }

        public string Owner
        {
            get { return (string)ViewState["Owner"]; }
            set { ViewState["Owner"] = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            // Most of our references are from your examples
            // Other reference: 
            /*
             * https://www.c-sharpcorner.com/article/dataview-in-C-Sharp/
             * https://forums.asp.net/t/2139408.aspx?access+a+dropdownlist+value+from+a+web+user+control
             * https://www.guru99.com/asp-net-controls-webforms.html
             */

            // Declare all codes as List<string>
            List<string> cssExample = new List<string>(new string[]{
                ".div3 {",
                "*float: left;",
                "*width: 100px;",
                "*height: 50px;",
                "*margin: 10px;",
                "*border: 3px solid #73AD21;",
                "}",
                ".div4 {",
                "*border: 1px solid red;",
                "*clear: left;",
                "}"
            });

            List<string> myCssCode = new List<string>(new string[]{
                ".sideleft {",
                "*float: left;",
                "*width: 18 %;",
                "*position: absolute;",
                "*background-color: #a8fbff;",
                "*margin: 5px;",
                "}",
                ".sideform {",
                "*width: 70%;",
                "*float: right;",
                "*background-color: #6e7f80;",
                "*font-weight: bold;",
                "}",
                "main {",
                "*width: 70%;",
                "*float: right;",
                "*background-color: #6e7f80;",
                "}",
                "#foot {",
                "*clear: both;",
                "*width: 100%;",
                "}"
            });

            List<string> jsEx = new List<string>(new string[]{
                "var cars = [\"BMW\", \"Volvo\", \"Saab\", \"Ford\"];",
                "var text = \" \"; ",
                "*for(var i = 0; i < cars.length; i++)",
                "*{",
                "**text += cars[i] + '<br>';",
                "*}",
            });

            List<string> myJs = new List<string>(new string[]{
                "var book1 = ' Ender's Game '",
                "var book2 = ' Catcher and the Rye '",
                "var book3 = ' A Clockwork Orange '",
                "var book4 = ' 1984 '",
                "var book5 = ' Game of Thrones '",
                "var book6 = ' Lord of the Rings '",
                "var book7 = ' The Hobbit '",
                "var book8 = ' It '",
                "var book9 = ' A Clash of Kings '",
                "var book10 = ' Norse Mythology '",
                "*//Assigning all book variables to an array",
                "var bookArray = [book1, book2, book3, book4, book5, book6, book7, book8, book9, book10];",
                "*//assigning a variable for the loop",
                "var i = 0;",
                "*//While loop runs as long as the condion is met",
                "while (i < bookArray.length)",
                "{ **//condition that must be met (input less than 10)",
                "**console.log(' Book ' + (i + 1) + ':' + bookArray[i]); //sends the book number and title to the console",
                "**i++; //incrementor meaning we are adding to something",
                "}",
                "*/*for(var i = 0; i < bookArray.length; i++) {",
                "*console.log('Book ' + (i + 1) + ':' + bookArray[i]);",
                "*} */",
                "**//Prompts question",
                "var userInput = prompt('What top 10 book would you like?', 'Pick a number: 1-10');",
                "*//Input validation logic (Only accepts numbers between 1 and 10)",
                "if (userInput > 10 || userInput < 1 || userInput === '' || userInput === 'Pick a number: 1-10' || userInput === isNaN)",
                "{",
                "**alert('Please enter a number between 1 and 10');",
                "}",
                "else",
                "{",
                "**alert('Book ' + userInput + ':' + bookArray[userInput - 1]); //Runs if conditions are met, subtracting 1 from userinput",
                "}"
            });

            List<string> sqlEx = new List<string>(new string[]{
                "SELECT student, assignment",
                "FROM assignments",
                "LEFT JOIN students",
                "ON assignments.class = students.class",

                "DATA",
                "Birinder  ||  Paper",
                "Amandeep  ||  Exam",
                "(null)    ||  Lab",
            });

            List<string> mySql = new List<string>(new string[]{
                "SELECT invoice_id, v.vendor_name",
                "FROM invoices i",
                "FULL JOIN vendors v",
                "ON v.vendor_id = i.vendor_id"
            });

            // Error message if user ever commits a typographical error 
            // or enters a code that's not available
            List<string> codeNotFound = new List<string>(new string[]{
                "**Data not found:",
                "**The requested data cannot be found!",
            });

            //init codeData class
            CodeData myCodeView = new CodeData();
            ProcessCode outputCodeView = new ProcessCode(myCodeView);

            //convert content to lower string to prevent error
            Code = Code.ToLower();

            //switch value to execute appropriate code
            switch (Code)
            {
                case "css_example":
                    myCodeView.SourceCode = cssExample;
                    break;
                case "my_css_code":
                    myCodeView.SourceCode = myCssCode;
                    break;
                case "my_javascript":
                    myCodeView.SourceCode = myJs;
                    break;
                case "javascript_example":
                    myCodeView.SourceCode = jsEx;
                    break;
                case "sql_example":
                    myCodeView.SourceCode = sqlEx;
                    break;
                case "my_sql":
                    myCodeView.SourceCode = mySql;
                    break;
                default:
                    myCodeView.SourceCode = codeNotFound;
                    break;
            }

            // Display Code Owner
            //convert to lower string first
            string convertLower = Owner.ToLower();

            if (convertLower.Equals("me") == true)
            {
                codeOwner.InnerHtml = "My Code";
            }
            else
            {
                codeOwner.InnerHtml = "Code Example by: " + Owner;
            }

            // Output the codebox
            myCodeBoxUC.DataSource = outputCodeView.PrintCode();
            myCodeBoxUC.DataBind();
        }
    }
}
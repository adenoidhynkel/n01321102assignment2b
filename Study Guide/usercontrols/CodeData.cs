﻿using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Study_Guide.usercontrols
{
    public class CodeData
    {
        private List<string> sourceCode = new List<string> { };

        public CodeData()
        {

        }

        public List<string> SourceCode
        {
            get { return sourceCode; }
            set { sourceCode = value; }
        }
    }
}
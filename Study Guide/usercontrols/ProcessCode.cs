﻿using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Study_Guide.usercontrols
{
    public class ProcessCode
    {
        public CodeData codeData;

        public ProcessCode(CodeData cd)
        {
            codeData = cd;
        }

        public DataView PrintCode()
        {
            /*
             * We Decided to implement the assignment using classes to reduce clutter and to take advantage and learn more with objects and classes 
             * creating a function with a list string parameter would have been enough but we like to try other things this may be better if we were to add more features/functionalities
             */

            //Most of our references are from your examples
            // Other reference: 
            /*
             * https://www.c-sharpcorner.com/article/dataview-in-C-Sharp/
             */

            // declaring title headings for Line and Code
            // we've seen on the link above that we can declare the 
            // data column object parameters in one line
            // we are not sure if there are any effects on the performance
            // but we think that you separated these to help us understand the code better

            DataColumn dataColIndex = new DataColumn("Line", System.Type.GetType("System.Int32"));
            DataColumn dataColCode = new DataColumn("Code", System.Type.GetType("System.String"));

            // declaring the table to contain the data
            DataTable dataCodeTable = new DataTable();

            // declare the row for index and code
            DataRow dataCodeRow;

            // add the columns on the table
            dataCodeTable.Columns.Add(dataColIndex);
            dataCodeTable.Columns.Add(dataColCode);

            // copy the list string into codeBlock
            // copying a list string reference : https://stackoverflow.com/questions/1952185/how-do-i-copy-items-from-list-to-list-without-foreach

            List<string> codeBlock = new List<string>(codeData.SourceCode);
            int counter = 0;
            string switchCode;

            // store the contents of string list using for each
            foreach (string snippet in codeBlock)
            {
                // create a new row in the table
                dataCodeRow = dataCodeTable.NewRow();

                // assign current counter value to index columnn value
                dataCodeRow[dataColIndex] = counter;

                // convert the string so that asp won't parse the HTML TAGS
                switchCode = System.Net.WebUtility.HtmlEncode(snippet); ;

                // replace asterisks with actual HTML non-breaking space 
                // more &nbsp;'s = more spacing
                switchCode = switchCode.Replace("*", "&nbsp;&nbsp;&nbsp;&nbsp;");

                // assign the code line to current row
                dataCodeRow[dataColCode] = switchCode;

                // increment the counter
                counter++;

                // add rows to table
                dataCodeTable.Rows.Add(dataCodeRow);
            }

            // create a data view with the table
            DataView dataPrintCode = new DataView(dataCodeTable);
            // return the dataview
            return dataPrintCode;
        }
    }
}